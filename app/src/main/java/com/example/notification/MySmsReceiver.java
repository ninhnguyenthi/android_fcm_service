package com.example.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

public class MySmsReceiver extends BroadcastReceiver {

    //    private static final String TAG = MySmsReceiver.class.getSimpleName();
    public static final String pdu_type = "pdus";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        String sms = "android.provider.Telephony.SMS_RECEIVED";

        if (intent.getAction().equals(sms)) {
            Bundle bundle = intent.getExtras();

            Object[] objects = (Object[]) bundle.get(pdu_type);

            SmsMessage[] messages = new SmsMessage[objects.length];
            for (int i = 0; i < objects.length; i++) {
                messages[i] = SmsMessage.createFromPdu((byte[]) objects[i]);

            }

            Toast.makeText(context, messages[0].getMessageBody(), Toast.LENGTH_LONG).show();


        }
    }
}
